muryokusho is the infrastructure code for the rikugan micro service cluster

# services

gojo - app
satoru - api
mugen - db

ao - news api
aka - csv|docs api
murasaki - llm api

# infra

muryokusho - infra
rikugan - cluster